*ITMO java pizza back

- register user(POST):
/api/registration
{
	"name": "user",
	"email": "asd@asd.asd",
	"password": "aaa"
}

- login (POST) application/x-www-form-urlencoded:
/auth/login?username=asd@asd.asd&password=aaa

- Place order (POST)
/api/orders
{     
    "status": null,
    "totalPrice": 2.345235233E8,
    "createdAt": null,
    "userComment": "",
    "adminComment": "",
    "deliveryCost": 5.0,
    "currency": "EU",
    "payment": "cash",
    "address": "sadfs",
    "customerName": "asdasd",
    "phone": "asdas",
    "userId": 1,
    "items": [
        {
            "id": {
                "menuId": 1               
            },
            "menu": {
                "id": 1,
                "name": "margarita",
                "price": 10.1,
                "description": "sdfsdfsdfsdfsdf",
                "enabled": true,
                "imageName": "sdfsdf",
                "originalImageName": "sdfsdf"
            },
            "qty": 3,
            "size": "3"
        }
    ]
}	

- Retrieve all orders(GET)
/api/orders

- Retrieve all menu(GET)
/api/menu

- Add new menu item(POST)
/api/menu
{
    "name": "asd",
    "price": 123123.0,
    "description": "asd",
    "enabled": true,
    "imageName": "",
    "originalImageName": "asd"
}

