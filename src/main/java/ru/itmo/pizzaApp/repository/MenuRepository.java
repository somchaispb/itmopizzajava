package ru.itmo.pizzaApp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itmo.pizzaApp.domain.Menu;

import java.util.List;

public interface MenuRepository extends JpaRepository<Menu,Integer> {
    List<Menu> findAllByEnabledIsTrue();
}
