package ru.itmo.pizzaApp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itmo.pizzaApp.domain.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User,Integer> {
    Optional<User> findByLogin(String login);
}
