package ru.itmo.pizzaApp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itmo.pizzaApp.domain.Status;

public interface StatusRepository extends JpaRepository<Status, Integer> {
}
