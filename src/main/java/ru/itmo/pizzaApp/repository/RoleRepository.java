package ru.itmo.pizzaApp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itmo.pizzaApp.domain.Role;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Integer> {

    Optional<Role> findByName(String name);
}
