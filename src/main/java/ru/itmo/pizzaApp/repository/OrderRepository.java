package ru.itmo.pizzaApp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itmo.pizzaApp.domain.Orders;

public interface OrderRepository extends JpaRepository<Orders, Integer> {
}
