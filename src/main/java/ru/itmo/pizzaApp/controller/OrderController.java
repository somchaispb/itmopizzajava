package ru.itmo.pizzaApp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.itmo.pizzaApp.domain.Orders;
import ru.itmo.pizzaApp.domain.User;
import ru.itmo.pizzaApp.service.OrderService;

import java.util.List;


@RestController
@RequestMapping("/orders")
public class OrderController {

    @Autowired
    OrderService orderService;

    @GetMapping
    public List<Orders> getOrders()
    {
        System.out.println(123);
        return orderService.findAllOrdersForUser();
    }


    @PostMapping
    public Orders postOrders(@RequestBody Orders order)
    {
        return orderService.saveOrder(order);
    }

    @DeleteMapping("/{id}")
    public void deleteOrder(Orders order)
    {
        orderService.deleteOrder(order);
    }

    @PutMapping("/{id}")
    public void updateOrder(Orders order)
    {
        orderService.updateOrder(order);
    }


}
