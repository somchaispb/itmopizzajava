package ru.itmo.pizzaApp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.itmo.pizzaApp.domain.Menu;
import ru.itmo.pizzaApp.service.MenuService;

import java.util.List;

@RestController
@RequestMapping("/menu")
public class MenuController {

    @Autowired
    MenuService menuService;

    @GetMapping("")
    public List<Menu> getMenu() {
        return menuService.findAllMenuForUser();
    }

    @PostMapping("")
    public Menu saveMenu(@RequestBody Menu menu) {
        return menuService.saveMenu(menu);
    }

    @GetMapping("/all")
    public List<Menu> generateMenu(){
        return menuService.generateMenu();
    }
}
