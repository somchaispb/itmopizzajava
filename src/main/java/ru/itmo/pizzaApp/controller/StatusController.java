package ru.itmo.pizzaApp.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itmo.pizzaApp.domain.Status;
import ru.itmo.pizzaApp.service.StatusService;

import java.util.List;

@RestController
@RequestMapping("/statuses")
public class StatusController {

    @Autowired
    StatusService statusService;

    @GetMapping
    public List<Status> getStatuses()
    {
        return statusService.findAllStatuses();
    }
}
