package ru.itmo.pizzaApp.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin")
public class AdminController {


    @GetMapping("/menu")
    public void getMenu()
    {

    }

    @PutMapping("/menu/{id}")
    public void updateMenu()
    {

    }

    @GetMapping("/orders")
    public void getOrders() {

    }

    @GetMapping("/orders/{id}")
    public void updateOrders() {

    }
}
