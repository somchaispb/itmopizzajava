package ru.itmo.pizzaApp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itmo.pizzaApp.domain.Role;
import ru.itmo.pizzaApp.domain.User;
import ru.itmo.pizzaApp.dto.UserDto;
import ru.itmo.pizzaApp.service.UserService;

@RestController
@RequestMapping("/registration")
public class RegisterUserController {

    @Autowired
    UserService userService;

    @PostMapping
    public User registerUser(@RequestBody UserDto userDto){
       return userService.tryRegister(userDto);
    }

    @PostMapping("/role")
    public Role createRoleUser(){
        return userService.createUserRole();
    }
}
