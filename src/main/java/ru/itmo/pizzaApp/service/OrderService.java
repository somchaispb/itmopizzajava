package ru.itmo.pizzaApp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itmo.pizzaApp.domain.OrderMenu;
import ru.itmo.pizzaApp.domain.Orders;
import ru.itmo.pizzaApp.repository.MenuRepository;
import ru.itmo.pizzaApp.repository.OrderRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class OrderService {

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    MenuRepository menuRepository;

    public List<Orders> findAllOrdersForUser() {
        return orderRepository.findAll();
    }

    public Orders saveOrder(Orders order) {
        List<OrderMenu> orderMenus = order.getItems();
        for (OrderMenu orderMenu: orderMenus){
            Integer id = orderMenu.getMenu().getId();
            orderMenu.setMenu(menuRepository.findById(id).get());
        }
        return orderRepository.save(order);
    }

    public void deleteOrder(Orders order) {
        orderRepository.delete(order);
    }

    public Orders updateOrder(Orders order) {
        Orders updatedOrder = order;
        if (order.getId() != null)
            updatedOrder = orderRepository.save(order);
        return updatedOrder;
    }
}
