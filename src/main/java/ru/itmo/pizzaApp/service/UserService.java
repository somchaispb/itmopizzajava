package ru.itmo.pizzaApp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import ru.itmo.pizzaApp.domain.Role;
import ru.itmo.pizzaApp.domain.User;
import ru.itmo.pizzaApp.dto.UserDto;
import ru.itmo.pizzaApp.repository.RoleRepository;
import ru.itmo.pizzaApp.repository.UserRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    MyUserDetailService myUserDetailService;

    @Autowired
    RoleRepository roleRepository;

    public User tryRegister(UserDto userDto) {
        validateUserDto(userDto);
        if (isLoginAvailable(userDto.getEmail())){
            register(userDto);
            return (User) myUserDetailService.loadUserByUsername(userDto.getEmail());
        } else {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
        }
    }

    private void validateUserDto(UserDto userDto){
        if (userDto == null || userDto.getEmail() == null || "".equals(userDto.getEmail()) || userDto.getPassword() == null || "".equals(userDto.getPassword()) ){
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
        }
    }

    private Boolean isLoginAvailable(String login){
        Optional<User> optionalUser = userRepository.findByLogin(login);
        if (optionalUser.isPresent()){
            return false;
        } else {
            return true;
        }
    }

    private void register(UserDto userDto){
        User user = new User();
        user.setLogin(userDto.getEmail());
        user.setPassword(getPasswordEncoder().encode(userDto.getPassword()));
        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        user.setCredentialsNonExpired(true);
        user.setEnabled(true);
        user.setName(userDto.getName());
        prepareRole();
        user.setAuthorities(Collections.singletonList(getRole()));
        userRepository.save(user);

    }

    private BCryptPasswordEncoder getPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }

    private Role getRole(){
        Optional<Role> role = roleRepository.findByName("user");
        if (role.isPresent()){
            return role.get();
        } else {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
        }
    }

    public Role createUserRole() {
        Role role = new Role();
        role.setName("user");
        role.setCreatedAt(LocalDateTime.now());
        return roleRepository.save(role);
    }

    private void prepareRole(){
        Optional<Role> role = roleRepository.findByName("user");
        if (!role.isPresent()){
            createUserRole();
        }
    }
}
