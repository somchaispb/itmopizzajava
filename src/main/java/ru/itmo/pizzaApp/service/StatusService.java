package ru.itmo.pizzaApp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itmo.pizzaApp.domain.Status;
import ru.itmo.pizzaApp.repository.StatusRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class StatusService {

    @Autowired
    StatusRepository statusRepository;

    public List<Status> findAllStatuses() {
        if (statusRepository.findAll().isEmpty()){
            fillStatuses();
        }
        return statusRepository.findAll();
    }

    private void fillStatuses(){
        List<String> statusesNames = Arrays.asList("created","payed","delivery","delivered");
        List<Status> statuses = new ArrayList<>();
        for (String statusName : statusesNames){
            statuses.add(createStatus(statusName));
        }
        statusRepository.saveAll(statuses);
    }

    private Status createStatus(String name){
        Status status = new Status();
        status.setName(name);
        return status;
    }


}
