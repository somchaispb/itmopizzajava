package ru.itmo.pizzaApp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itmo.pizzaApp.domain.Menu;
import ru.itmo.pizzaApp.repository.MenuRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class MenuService {

    @Autowired
    MenuRepository menuRepository;


    public List<Menu> findAllMenuForUser() {
        return menuRepository.findAllByEnabledIsTrue();
    }

    public List<Menu> findAllMenuForAdmin() {
        return menuRepository.findAll();
    }

    public Menu saveMenu(Menu menu) {
        return menuRepository.save(menu);
    }

    public List<Menu> generateMenu() {

        List<Menu> menus = new ArrayList<>();

        Menu menu = new Menu();
        menu.setOriginalImageName("sdfsdf");
        menu.setPrice(10.1);
        menu.setName("margarita");
        menu.setImageName("sdfsdf");
        menu.setEnabled(true);
        menu.setDescription("sdfsdfsdfsdfsdf");
        menus.add(menu);

        Menu menu1 = new Menu();
        menu1.setOriginalImageName("sdfsdf");
        menu1.setPrice(10.1);
        menu1.setName("margarita");
        menu1.setImageName("sdfsdf");
        menu1.setEnabled(true);
        menu1.setDescription("sdfsdfsdfsdfsdf");
        menus.add(menu1);

        return menuRepository.saveAll(menus);
    }
}
