package ru.itmo.pizzaApp.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "Orders")
public class Orders {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne
    @JoinColumn(name = "statusId", referencedColumnName = "id", insertable = false, updatable = false)
    private Status status;

    private Double totalPrice;
    private LocalDateTime createdAt;
    private String userComment;
    private String adminComment;
    private Double deliveryCost;
    private String currency;
    private String payment;
    private String address;
    private String customerName;
    private String phone;
    private Integer userId;

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER,mappedBy = "orders")
    @JsonManagedReference
    private List<OrderMenu> items;

}
