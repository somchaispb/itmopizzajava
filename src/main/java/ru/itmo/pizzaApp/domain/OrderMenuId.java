package ru.itmo.pizzaApp.domain;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Setter
@Getter
public class OrderMenuId implements Serializable {
    @Column(name = "menuId")
    private Integer menuId;

    @Column(name = "orderId")
    private Integer orderId;

    public OrderMenuId() {}

    public OrderMenuId(
            Integer menuId,
            Integer orderId) {
        this.menuId = menuId;
        this.orderId = orderId;
    }

    //Getters omitted for brevity

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass())
            return false;

        OrderMenuId that = (OrderMenuId) o;
        return Objects.equals(menuId, that.menuId) &&
                Objects.equals(orderId, that.orderId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(menuId, orderId);
    }
}
