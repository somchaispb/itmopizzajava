package ru.itmo.pizzaApp.domain;


import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Getter
@Setter
@Table(name = "OrderMenu")
public class OrderMenu
{

    @EmbeddedId
    private OrderMenuId id = new OrderMenuId();

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference
    @MapsId("orderId")
    private Orders orders;

    //@ManyToOne(cascade = CascadeType.ALL)
    @ManyToOne
    @MapsId("menuId")
    private Menu menu;

    private Integer qty;
    private String size;

//    private OrderMenu(){}
//
//    public OrderMenu(Menu menu, Order order) {
//        this.menu = menu;
//        this.order = order;
//        this.id = new OrderMenuId(order.getId(), menu.getId());
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//
//        if (o == null || getClass() != o.getClass())
//            return false;
//
//        OrderMenu that = (OrderMenu) o;
//        return Objects.equals(order, that.order) &&
//                Objects.equals(menu, that.menu);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(menu, order);
//    }

}
